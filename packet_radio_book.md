PACKET RADIO
============

Versuch einer Zusammenfassung des Buchs "Packet Radio" von Wolf-Dieter Roth DL3MCD (sk) ISBN 3-88180-366-1

Hier und da Erweiterungen..

Kurzübersicht
-------------

* Paket Radio ist Datenübertragung über Funk, aus Zeiten vor Mobilfunk und WLAN
* Für Amateurfunk und CB-Funk verwendet
* Daten können über Digipeater vermittelt werden
* auch transparente Netzwerke möglich
* Es gab Mailboxen mit Nachrichten und Informationen - wie BBS über Telefon

### Geschwindigkeiten/Bänder
Es sind jeweils die aktuellen Frequenzlisten zu suchen.

#### Kurzwelle

300 Baud mit SSB, um die 14.1 Mhz im 20m Band

Packet-Kollisionen und diese vermeiden:
bei KW/300Baud sollte man unter 30 bis 64 Zeichen pro Paket bleiben.

Rauschsperre als Kanalbelegungsfunktion funktioniert auf KW/SSB nicht.

Nicht auf KW üblich : Digipeater, Mehrfachverbindungen

der TNC wiederholt, je nach Einstellung, Pakete bis zu 4 mal bis eine Bestätigung kommt.

#### 10m/11m
im 10m-Band 29.2..29.3 Mhz, FM 1200 Baud

im 11m-Band auch 1200 Baud, jeweilige CB-Funk Datenkanäle

#### 2m Band
APRS.

im 2m-Band 1200 Baud

144.8 bis 144.99 Mhz. hier sind auch andere digimodes, wie SSTC, Fax, RTTY, AMTOR etc

2m ist nicht so anfällig für hindernisse, wetter etc wie 70cm

#### 70cm Band
Im 70cm-Band 1200 Baud oder 9600 Baud

430.6..430.675, 433.625..433.775, 438.025..438.275 MHz

In Deutschland 7.6MHz Frequenzablage für Duplex

FULLDUP/duplexmode bei TNCs führt zu kollisionen

Funkgerät sollte 12.5khz oder 25khz Raster haben. 

Alte Quarzgeräte brauchen meist entsprechende neue Quarze. Moderne PLL Geräte sind nicht betroffen.

PMR/LPD sind für Allgemeinheit nutzbare Kanäle. Problem: keine Modifikation an Funkgeräten zugelassen, daher in der Regel nur 1200Baud möglich

#### ALOHA DAMA CSMA

* ALOHA hier wir einfach gesendet, in der Hoffnung, mit niemanden zu kollidieren. 
* DAMA Erweiterung des PR-Protokolls, "Demand Assigned Multiple Access", ein Netzknoten verteilt "Senderechte", wer als nächstes was sagen darf.
* CSMA Einfache Regel, erst zu schauen dass keiner gerade sendet "Carrier Sensed Multiple Access". Funktioniert nicht, wenn zwei entfernte Stationen gleichzeitig zu einem Knoten sprechen.


### CB vs Afu
* CB, PMR, LPD und andere ISM sind Lizenzfrei, hier gibt es Kanäle für Datenübertragung
* Auf Amateurfunkbändern braucht man entsprechende Lizenz und Zulassung
* Viele Software für Packet Radio ist nicht für CB etc geeignet, setzen Amateurfunkrufzeichen voraus
* Ebenso verlangen neben Software auch TNC's oft Angabe des Rufzeichens

Exkurs: Vorgänger und andere Modes
----------------------------------

Packet Radio verwendet das AX.25 Protokoll

Andere alte Datenübertragungsarten
* Morsen
* RTTY = Fernschreiben 
* Funkfernschreiben hatte as Standard 45,45 Baud
* "Hell-Schreiber"
* Übertragung in ASCII, 7bit oder 8bit, mit und ohne Parity
* Baudot Kodierung
 * "Amtor" ist mit Baudot aber mit Parity
 * "Sitor" war Variante von Siemens
 * "NAVTEX" war Amtor für Schifffahrt
* tty Erweitungen: XMODEM, KERMIT, ZMODEM, X.42
* mit Datenpaketen: X.25 (datex-P-Netz), X.25 war kabelgebunden
* ALOHA Funknetz verband mehrere Stationen der Uni Hawaii mit 9600 Baud
* Auf Kurzwelle "PACTOR", ist mit CRC, Kompression, "memory-ARQ", kleine Pakete
* Militär: "Clover" von Firma "HAL", waren Steckkarten, teuer und selten
* Wetterfax "WEFAX"
* AX.25 wird auch bei IR-Übertragungen eingesetzt (Drucker/peripherie im Raum anbinden)
* modacom und mobitex: ehemalige kommerzielle Datenfunknetze im Paketradio

Geschichte
*1978 Vancouver Amateur Digital Communications Group VADCG entwickelt ein PR-Protokoll und erste TNC. Das "Vancouver-Protovol" /VADCG verwendete noch fortlaufende Nummern als Addressen
*1981 Tucson Amateur Paket Radio Corporation TAPR entwicket AX.25 und TAPR-TNC. TAPR/AX.25 verwendet Rufzeichen im Adressfeld. TNC2 wird entwickelt
*1984 wird AX.25 publiziert. Dazu relativ freie Nachbaurechte für TNC2
*1989 Prototypen schaffen auf 900mhz/2.4Ghz mit AX.25 38.4kBaud bis 230kBaud


Terminal Node Controller
------------------------
* wird an Funkgerät gekoppelt
* Auf Rechnerseite RX und TX, auf Funkgerätseite Mic, Spk, PTT
* Standalone Gerät für Abwicklung Packetradio
* RS-232/V.24 Schnittstelle zum Rechner
* TNC hat integriertes Modem, Firmware, Speicher
* Je nach Firmware unterschiedliche Features
* Umbenannter Betrieb ist heikel: eigentlich nicht gestattet, aber oft geduldet

### TNC Hardware
TNC1 - erster TNC

integrieres netzteil, 25kb rom, 6kb ram, 6809 controller. WA8DED firmware

GLB PK1 - auch alt, anderer kommandosatz

TNC2 - von TAPR. man sollte gleich 9k6 Variante nehmen

TNC2A und TNC2C - Nachbauten vom TNC2. "C" ist mit Cmos, statt 2W nur 0.5W Verbrauch.

TAPR war Standard als Firmware für TNC2 gewesen. WA8DED als Alternative.

Eisch-TNC von DC8SE ist auch ein TNC2-klon

Landolt-TNC - Die Originalfirmware wurde eingedeutscht, machte Probleme mit pc-software

(gab es auch: DK9SJ-TNC)

Pakratt PK-232 von Fa AEA - kann auch Morse, RTTY, Amtor, mit anderen Roms auch mehr

AEA hatte noch mehr im Angebot, war teure Oberklasse für PR

DSPCOM, KAM "Kantronics all mode" - weitere TNCs mit DSPs und mehr Funktionen

MFJ 1270, 1274, 1278 von MFJ

OE5DXL hat mehrere TNCs mit "DXL" gebaut, brauchen eigene Dos-software DXLCOM, Parallelport

TNC3 - nicht auf Markt gekommen, von NORD><LINK

TNC3S mit 68302 prozessor, auch vielseitig

Weitere Varianten:
*PRIMUS - flop, nur altes AX.25, einige bugs.
*DRSI - KISS mode, TSR treiber, WA8DED kompatibel, nicht TAPR kompatibel
*kein Kontroller im TNC: GLB-TNC

ein reiner KISS-TNC kam nie auf den markt

### TNC in Software
Nur ein Modembaustein auf einer Platine, rest an PAD etc wird im Rechner gemacht

Alte Software und Software für Homecomputer verwenden evtl Vancouver Protokoll oder Altes AX.25 v1 Protokoll

Baycom/Digicom: Ein Serielles Modem, Hersteller war Baynet, es gab Software für C64 oder PC

Trick von Baycom-modem: RTS/CTS für Datenübertragung verwendet. 

Es gab Baycom Varianten mit Parallelport und Isa-Steckkarten, Highspeed mit bis zu 76.8 kBaud

PMP, "poor man's packet" Variante

TFPCX ist ein Dostreiber für TNC2, die das KISS Protokoll von WA8DED beherrschen, simuliert dann ein Baycom Modem

Für win95: Flexnet 

### Digipeater
Digipeater vermitteln weiter, es gibt Digipeater auf verschiedenen Ebenen 

Eigentlich jede PR Station kann digipeaten. (OSI level 2)

Befehl:
 C ZIELRUFZEICHEN via DIGIPEATERRUFZEICHEN1, DIGIPEATERRUFZEICHEN2
 
Der Digipeater verbindet dann den Sender mit dem TNC vom Ziel

Jeder digipeater setzt bei seinem Rufzeichen ein Bit zur Markierung: hab das selber schon gesendet

Maximal 8 Digipeater passen ins AX.25 Protokoll

TheNet und Flexnet haben Varianten bzw Verfahren mitunter Anders, OSI Level 3

Manche Digipeater können auch weitere TNC ansprechen, bzw. die Software erlaubt Daisy-Chaining

TheNet:
*NET/ROM/TheNet von WA8DED (OSI level 3), nicht public domain
*hier kennen die digipeater alle zielstationen, die sie erreichen können
*TheNet von Nord><link ist nachbau, kostenlos, läuft auf TNC2
*zwei TheNet TNC2 sind direkt verknüpfbar (?)
*An Rufzeichen wird noch eine SSID angehängt, 0 bis 15, "user" sollten nur bis 7 verwenden,
*im digipeater net wird diese SSID von 15 abgezogen, damit erkannt wird, dass packet im digipeater netz ist und nicht ausserhalb
*man braucht SSID im thenet digipeater netz, um mehrfachverbindungen zu machen
*SSIDs gibt es nur bei baycom, digicom, und im kiss-mode
*die Knoten machen Node-Rundrufe, um andere knoten zu finden
*befehle: USER, NODES, INFO, ROUTES, PARMS, CQ
*TheNetNode ist noch umfangreicher, läuft auf PC
*XNET läuft auf größeren TNC3
*Flexnet ist ebenfalls umfangreich: RMNC/Flexnet Rhein Main Network Controller mit 19" Einschüben
*dann DXL von OE5DXL und Baycom Node

### Firmware
WA8DED. recht unkonfortabel, "terminalmodus" für direkte bedienung mit terminal
kann aber auch "host mode", dann übernimmt pc aufgaben.

The Net-Node TNN von nordlink, in Verbindung mit KISS mode als "besserer" TNC

TAPR Firmware:
* TAPR-eproms geben "streamswitch" Meldungen aus, verwirrend für User und PC.
* TAPR ist aber besser für Anfänger geeignet als WA8DED
* TAPR will nach Umkonfiguration erst ein "RESTART" bevor Änderungen wirksam werden
* TAPR hat modi: CONVERS TRANSPARENT COMMAND
* TAPR Mode:
 * Daten kommen ohne Rufzeichen raus, unsortiert
 * in diesem Modus die wenigsten Eingriffsmöglichkeiten
 * einfacher, Nachrichten zu speichern als bei WA8DED
 
terminal Mode
*standalone Modus, TNC sammelt Nachrichten, Rechner kann ausgeschaltet sein.
 
KISS Mode:
* KISS-Mode ist eigentlich halbe softwarelösung
* alle AX.25 daten kommen in den pc
* TNC ist quasi nur ein dummes modem
* features aus TNC übernimmt software:
 * DAMA
 * Framesammler (ab AX.25 v2.4)
 * multiconnect
 * gateway
* KISS gibt es in folgenden firmwares
 * TAPR ab 1.1.6
 * The Firmware (WA8DED kompatibel) von Nordlink ab 2.1
 * integriert in den größeren TNCs
 * NICHT bei WA8DED
* KISS Mode sollte man von Software aktivieren lassen. Aktivieren: 
 * KISS ON [enter] 
 * RESTART [enter]
 
converse mode:
* AX.25 Pakete können als "UI"-Päckchen markiert werden, unprotokolliert,
* wie UPD wird keine Bestätigung erwartet.
* mit dem TNC-converse mode können so ohne Connect-Befehl Gesprächsrunden gemacht werden, jeder kann senden, jeder kann alles lesen was sonst kommt.
* je nach TNC Einstellung werden Pakete mit falschem CRC verworfen oder nicht
* alternative: es gibt extra TNC firmware versionen für Converse Runden, damit diese Pakete weitergeleitet werden, dann aber mit Connects
* in Conversknoten wurden dann Kanäle geschaffen, da zu schnell zu viel Traffic entsteht

Weitere Firmware:
* UIDIGI - aus Italien
* vereinzelt gab es noch andere Firmwares, Protokolle

Modem und Funkgerät
-------------------

Zuerst auf 2/3 Lautstärke stellen, dann Rauschsperre einrichten (squelch)

* 300Baud: 200Hz frequenzhub
* 1200Baud: 1200Hz und 2200Hz Modemtöne, sollten gleich laut sein. Meist ist 2200Hz leiser.
* 9600Baud ist grenzwertig bei Funkgeräten, evtl Modifikation nötig, direkte Modulation/GMSK nötig
* Mitunter haben Funkgeräte gleich Schnittstelle für Modem
* Funkgerät braucht sonst Stecker für PTT, Mikrofon und Lautsprecher
* Modifikation von CB/PMR Funkgeräten ist nicht gestattet. 
* txdelay sollte kurz sein, ansonsten kann man das in Software oder im TNC anpassen
* Leistung: 1W reicht, 10W sind genug, über 25W stört man andere. Aber auf Kurzwelle sind 100W SSB denkbar
* Gibt auch extra Datenfunkgeräte
* Bei Satelliten zu beachten: dopplereffekt. Dazu FSK statt AFSK.

Antenne.. besonderes Thema. Oberwellen/Nebenaussendungen von TNC/PC beachten.

UART Schnittstelle
------------------
Erste Schritte Inbetriebname TNC:
*Rechner an, TNC an, kein Radio. 
*Schnittstelle und Baudrate einstellen, mit Terminal auf TNC einloggen
*RS232-Test: Drahtbrücke pin2 und pin3 - müsste ein Echo erzeugen, da TX mit RX kurzgeschlossen
*Zeichensalat? Baudrate falsch
*Hälfte der Zeichen falsch? Parity falsch eingestellt, wenn rechner 8n1 und TNC 7p1 ist
*TNC blind  auf 8n1 stellen:
 * AWLEN 8
 * PARITY 0
 * RESTART

rs232 25pol sub-d stecker
*1 erde/abschirmung
*2 senden txdata
*3 empfang rxdata
*4 sendeanfrage request to send RTS
*5 sendefreigabe clear to send CTS
*7 signalmasse
*8 trägererkennung carrier detect CD
*rest unbelegt

RTS,CTS und CD sind für Hardwarehandshake - oder für Baycom Modems

Im Softwarehandshake reicht eigentlch TX, RX, gnd

RX und TX kann evtl vertauscht sein, genauso CTS und RTS

Terminalprogramme
-----------------
Fangen Daten ab bzw schalten zwischen Kommando und Converse Modus um

* XTalk, Kermit, ProComm, Telix, Telemate
* * eigentlich für telefonbetrieb gedacht, in den Terminalmodus kommt man mit:
* * GO LOCAL
* YAPP - Yet Another Packet Programm
* * Dos, Hardwarehandshake, SplitScreen, verbindungen über digipeater, shareware, 
* PRTERM, TERM - YAPP weiterentwicklung für deutschen markt
* THP Turbo Host Packet, für Hostmode von QA8DED. THP ist Public domain mit freiem Quellcode. WA8DED Host mode
* TOP The Other Packet, ebenfalls Public Domain,  WA8DED Host mode
* PCT von DD6CV, kann mehrere TNCs ansteuern, WA8DED Host mode
* SP "Eskay Packet", "sigi's packet", "super packet", WA8DED Host Mode, logbuch, vor v7.0 public domain
* GP Graphic Packet von DH1DAE, VGA graphik, non-commercial-licence, WA8DED Host mode
* DIGICOM Komplettpaket für C64
* BAYCOM digicomklon für DOS, nur für baycom modems
* PRMON nur für baycom modems, nur mitlesen, public domain
* PRKISS/SUPERKISS - vor 4.0 public domain, KISS Mode. Bedienung ähnlich Digicom

####Atari ST
Packet-Master von DJ5JQ
####Amiga
Amiga-Host-Packet von DG6UL

HamPack III von DL8NN mit Bildübertragung

####Sinclair
für Sinclair QL hat DJ9EZ ein Programm geschrieben

Mailboxen
---------
Mailbox == BBS == RBBS == PBBS

Mailbox ist gleich BBS ist gleich RBBS ist gleich PBBS

"Radio/Paket Bulletin Board System"

* WA7MBL - können binärdaten speichern
 * CTEXT ist Begrüßungstext, hier sollte vermerkt sein, das Nachrichten gespeichert werden
 * "AWAY FROM KEYBOARD - PLS LEAVE MSG" oder sowas ist guter CTEXT wenn man unterwegs ist
 * der Begrüssungtext sollte auch auf die Software etc verweisen
* prompt ist meinst ">"
* READ und SEND zum lesen und schreiben
* HELP für hilfe
* LIST oder DIR für Anzeige
* idealerweise: auf eigene Festplatte alle Zugriffe mitschneiden, dann hat man dort die Hilfetexte
* sind alle Kanäle belegt, sollte ein "busy" zurückkommen
* Mailboxen machen oft Bakenaussendungen, in denen steht, für wen neue Nachrichten vorliegen
* Manche Mailboxen haben eine Erweiterung um Sprachspeicher mit DTMF Abruf, oder Sprachausgabe

Mailboxsoftware
* PR Mailboxenverbund
* W0rli war die erste für PR mit store und forward, mittels @ wird die Zielmailbox gewählt
* Empfänger kann mit ALTER FORWARD oder MYBBS seine Zielmailbox wählen
* WA7MBL für PC war Nachfolger. Bei beiden aber nur ein Zugang gleichzeitig.
* F6FBB Mailbox baut ebenfalls auf diesem system auf
* PACBOX von DF8HI (vom ccc) hat kein forwarding, aber 2 nutzer gleichzeitig.
 * (daraus entstand dann langsam nord><link, führte zur trennung vom ccc)
 * PACBOX trennt usernachrichten von bulletin boards (schwarzes brett)
* nach PACBOX entstand DIEBOX und BAYBOX
* DIEBOX von DF4AV, maximal 18user, mit @ werden lokale Nachrichten markiert, rest wird bundesweit gespiegelt
* Nachrichten in DIEBOX haben Lebenszeit, idr 14 Tage
* BAYBOX von DL8MBT von der bayrischen Packet Radio Gruppe BAPR oder von BayCom
* BAYBOX hat REPLY/REP befehl
* Eigene Mailboxen brauchen eigentlich eine Genehmigung von BNA, mit digipeaterrufzeichen
* Variante: Packet Cluster. Eine Convers Knoten mit Mailbox erweiterung, zum DX melden

Linux AX.25
-----------
Eigene Firmware/Protokoll "6pack" wurde entwickelt:
* 6pack ist noch direkter als KISS
* meldet Carrier Detekt etc direkt an PC
