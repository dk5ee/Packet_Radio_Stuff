
AX.25 Link Access Protocol for Amateur Packet Radio
===================================================

"klassischer" TAPR TNC-2 nutzt OSI Level 1, 2 und 7

Data-Link Service Access Point (DLSAP) 
--------------------------------------

Arten von einfachen Datenaustausch
* REQUEST um einen Dienst anzufragen
* INDICATION um auf eine Anfrage zu einem Dienst zu antworten
* CONFIRM eine Anfrage bestätigen

Segmenter: eine State Maschine, die größere Datenströme zerlegt und vereinigt.
Alle anderen Signale werden durchgelassen.

Frames
* Information (I) frame
* Unnumbered Information (UI) frame
* Unnumbered (U) frame
* Supervisory (S) frame
* (XID) frame
* (TEST) frame
* (FRMR) frame

Data-link State Machine
* startet und beendet Verbindungen zwischen zwei Stationen
* Datenaustausch bei verbindungslosen Transfers (UI Frame)
* Datenaustausch bei verbindungsorientieren Transfers (I Frame)
* Eine Data-link State Maschine pro Verbindung. Mehrere Verbindungen gleichzeitig möglich

Management Data-link State Maschine
* Für Parametereinstellungen zwischen Stationen
* ebenso eine MDLSM pro Verbindung

Link Multiplexer
* Damit mehrere Data-links über eine Verbindung laufen können
* Mit Rotationsalgorithmus
* Ein Link Multiplexer pro physikalischem Verbindungskanal

Physical State Maschine
* Schaltet das Funkgerät
* erhält auch Daten vom Funkgerät oder Modem
* Eine PSM pro physikalischem Verbindungskanal

Frame Aufbau
------------

Jeder Frame besteht aus Octets, ein vielfaches von 8bit 

Jeder Frame beginnt und endet mit einem Flag:
* 01111110
 
Dann folgt die Adresse mit Quelle und Ziel
* 112/224 bits - 14 bzw 28 bytes
* Eine Ziel und eine Quelladresse, jeweils 7 Zeichen: ein Rufzeichen dazu optional SSID
* Rufzeichen ist in Großbuchstaben mit Zahlen 7bit ASCII, aufgefüllt mit Spaces 0x20
* SSID ist eine 4 bit Integer, wird als Teil des letzten Zeichen der 7stelligen Adresse gesendet
* SSID-octet hat aufbau CRRSSID0, mit
 * C ist Flag für "Command" oder "Response", bei Zieladressen für AX.25, bei Sourceadresse für "LA PA"
  * wenn Ziel-C gleich '1' und Quell-C gleich '0' ist: "Command" Paket
  * wenn Ziel-C gleich '0' und Quell-C gleich '1' ist: "Response" Paket
  * ansonsten: Irgendwas vor AX.25 v2.0
 * Bei Repeater Adressen ist 'C' das 'H'-bit. Es wird von jedem Repeater, der es wiederholt hat, auf '1' gesetzt, sonst bleibt es. Der Repeater wiederholt nur Pakete, wo dieses Bit auf '0' ist.
 * Ein Repeater ändert nur diese Bit, berechnet dann die FCS neu, und sendet das Paket wieder aus
 * RR sind reservierte bits, in der Regel zweimal '1'
 * SSID ist die 4 bit StationsID
 * 0 ist das HDLC bit, 0 für "es folgt noch eine adresse", 1 für "das war die letzte adresse"
 * SSID von '0000' ist für die erste Station
* Optional zwei Repeater Adressen
* Die Adressen sind in 7bit ASCII, um 1 bit verschoben. das LSB ist '0' wenn noch eine Adresse folgt, '1' in der letzten Adresse, wenn keine Adresse folgt
* Das Bit nennt sich "HDLC address extension bit", es sollte beim letzten Zeichen der letzten Adresse '1' sein

Dann folgt das Controllfeld mit Frametype und zusätzlichen Werten
* 8/16 bits - 1 bzw 2 bytes
* drei arten von Control Fields: für I, S und U frames, jeweils Bits: 76543210 bzs FEDCBA9876543210
* I Frame RRRPSSS0
* S Frame RRRPss01
* U Frame MMMPMM11
* I Frame 16bit RRRRRRRP SSSSSSS0
* S Frame 16bit RRRRRRRP 0000ss01
* I Frames starten mit einer '0' im LSB, dem "control field"
* S Frames starten mit einer '1', gefolgt von einer '0'
* S Frames sind für Link Control wie ACK, retransmission requests von I-Frames, Link Layer Control. 
* S Frames haben kein Informations Feld/Info Field, Sequenz Nummern werden bei S-Frames nicht erhöht
* U Frames, also Unnummerierte, haben die ersten beiden Bits '1'. Dienen für weitere Kontrolle jenseits von S-Frames
* U Frames werden zum Verbindungsaufbau und -abbruch verwendet, und für Datenübertragung ausserhalb des normalen Flow Control
* U Frames können sowohl PID als auch Info Fields enthalten
* Die 8 bit Frames sind "modulo 8" Format, die 16 Bit Frames sind "modulo 128" Format
* Modulo 8 ist default
* jeder Frame bekommt eine fortlaufende Sequenznummer, maximal sieben Frames im Puffer
* Umschaltbar auf Modulo 128, dann sind maximal 127 "ausstehende" Frames möglich
* Bit 0 wird als erstes gesendet (rechts), Bit 7 bzw 15 wird als letztes gesendet
* Bits mit großem 'S' sind Send Sequenz Nummern. Bit 1 ist hier das LSB
* Bits mit 'R' sind Recieve Sequenz Nummern, Bit 5 bzw Bit 9 ist hier LSB
* Bits mit kleinem 's' sind Supervisory function bits, (für modulo 127 analog)
 * RRRP0001 RR Recieve Ready - System Ready To Receive. Der Sender des RR kann I-frames annehmen, hat I-frames empfangen, und ist nicht RNR. Wenn P gesetzt ist, fragt ein RR den Status der gegenstelle an
 * RRRP0101 RNR Recieve Not Ready - TNC Buffe Full. Der Sender kann keine I frames mehr annahmen, hat aber alle bisherigen Iframes emfangen. RNR endet mit UA, RR, REJ, SABM(E). kann mit RNR und P abgefragt werden
 * RRRP1001 REJ Reject - Out of Sequence or Duplicate. Alle Frames ab R sollen wiederholt werden, alle davor werden bestätigt. Kann auch mit REJ und P abgefragt werden. Nur ein REJ gleichzeitig
 * RRRP1101 SREJ Selective Reject - Request single frame repeat. Nur das Frame mit Nummer R soll wiederholt werden. Ist P gesetzt, sind alle Frames bis R-1 bestätigt, sond nicht. Mehrere SREJ gleichzeitig möglich, dann ist P '0'. SREJ und REJ schliessen sich aus
* Bits mit 'M' sind "Frame Modifier Bits" bei U-Frames
 * 011P1111 SABME Cmd - Connect Request Extended (Modulo 128). Alte Iframes werden verworfen. kein Info Field. Ack mit UA, Nack mit DISC, kein modulo128 mit FRMR
 * 001P1111 SABM Cmd - Connect Request (Modulo 8). Alte Iframes werden verworfen. kein Info Field. Ack mit UA, Nack mit DISC
 * 010P0000 DISC Cmd - Disconnect Request. kein Info Field. Ack mit UA, danach ist erst Verbindung getrennt, keine Bestätigungen mehr
 * 000P1111 DM Res - Disconnect Mode - System Busy or Disconnected. Antwort auf alles ausser UI und SABM(E), solange nicht verbunden. Wenn nicht verbunden, ist P=1, als antwort auf SABM ist P=0
 * 011P0011 UA Res - Unnumbered Acknowledge. kein Info Field. bestätigt empfangenen befehl
 * 100P0111 FRMR Res - Frame Reject
 * 000P0011 UI - Unnumbered Information Frame, mit PID und Info field, an normaler Flowcontrolle vorbei. Wenn P=1 wird DM, RR oder RNR als Antwort erwartet
 * 101P1111 XID - Exchange Identification - Negotiate features. Antwort ist XID oder FMFR oder UA wegen SABM. ISO 8885. in XID antwort sind Felder, beginnend mit
  * FI Format Identifier
  * GI Group Identifier
  * GL Group Length
  * Rest sind Parameter, ohne Parameter sind die standard Werte gemeint.
  * Parameter bestehen wiederum aus Parameter Identifier PI, Parameter Length PL und Parameter Value PV
 * 111P0011 TEST - Test. Antwort mit Test und dem gleichen Info Field, bzw. FRMR wenn Info Field zu groß ist.
* Bit 'P' ist das Poll/Final bit. Wird überall verwendet, auch für Anfragen nach eine unmittelbaren Antwort. 'P' und 'F' sind in diesem Dokument gleich
 * P ist '1' in UA oder DM Frame in Antwort auf ein SABM(E) oder DISC Frame, wo P auch '1' ist
 * P ist '1' in RR, RNR, REJ Frame in Antwort auf ein I oder S Frame, wo P auch '1' ist
 * P ist '1' im DM Frame in Antwort  auf ein I oder S Frame, wo P auch '1' ist
 * auch im Timeout Recovery ist P gleich '1' möglich
 * Ansonsten ist P gleich '0', wenn es nicht verwendet wird


In I-Frames folgt die PID. in U und S Frames fehlt die PID (Protocol Identifier field)
* 8 bits

Dann folgt das Info field, mit N von 0 bis 256. Nur in Frames vom Type I, UI, XID, TEST, FRMR
* N*8 bits - N bytes

Dann folgt die FCS Frame Chack Sequence nach ISO 3309. Nur diese Feld ist MSB. FCS ist über alles zwischen Flag und dem FCS
* 16 bits - 2 bytes
* Wenn FCS falsch ist, wird nichts gemacht, der Frame wird einfach verworfen, Sender muss erneut senden

und schliesslich das Flag 01111110

Alles zwischen zwei Flags ist das Paket, ein Flag kann gleichzeitig End- und Startflag sein.
Nur im Flag taucht die Bitfolge '111111' auf, bei allen anderen Feldern folgt nach fünfmal "1" ein Stuffbit "0", welches vom Empfänger verworfen wird


Jedes Feld/Octet wird LSB ausgeben, also Bit 0 zuerst, zum Schluss Bit 7,
ausser das FCS, hier wird bit 15 zuerst ausgegeben.

PID feld: Packet Identifier
* MSB..LSB Bedeutung
* yy01yyyy AX.25 layer 3 implemented
* yy10yyyy AX.25 layer 3 implemented
* 00000001 ISO 8208/CCITT X.25 PL
* 00000110 Compressed TCP/IP packet. Van Jacobson (RFC 1144)
* 00000111 Uncompressed TCP/IP packet. Van Jacobson (RFC 1144)
* 00001000 Segmentation fragment
* 11000011 TEXNET datagram protocol
* 11000100 Link Quality Protocol
* 11001010 Appletalk
* 11001011 Appletalk ARP
* 11001100 ARPA Internet Protocol
* 11001101 ARPA Address resolution
* 11001110 FlexNet
* 11001111 NET/ROM
* 11110000 No layer 3 protocol implemented
* 11111111 Escape character. Next octet contains more Level 3 protocol information.
* 00001000 Escape character. Next octet contains more Level 3 protocol information.
* YY00YYYY sonstige Y: reserviert für zukunft
* YY11YYYY sonstige Y: reserviert für zukunft 

Invalide Frames:
* kürzer als 136 bits inklusive Flags, also weniger als 13 bytes zwischen Flag und FCS
* Flag fehlt am Beginn und/oder am Ende
* nicht an octets ausgerichtet

Abbruch:
* mindestens 15mal '1' senden um Übertragung zu beenden, danach muss fürs nächste Paket ein Flag irgendwann kommen

Zeit überbrücken
* zwischen zwei Datenpaketen können ununterbrochen Flags gesendet werden, wenn der Sender aktiv gehalten werden soll

Verbindungsaufbau
-----------------
* TNC sendet SABM, startet Timer T1
* Wenn T1 überläuft: nochmal probieren, goto eins höher
* Wenn anderer TNC reagiert, sendet er UA Frame, und startet die State Engines neu. Alle internen Variablen werden auf '0' gesetzt
* Verbindungsaufbau wird versucht, bis Wert "N2" erreicht wird
* Wenn der andere TNC keine Verbindung aufbauen kann, wird DM Frame gesendet
* Wenn der TNC den DM Frame empfängt, wird Timer T1 beendet, und kein Informationstransfer begonnen
* Der anfragende TNC ignoriert alles Antworten des anfragenden TNCs ausser SABM, DISC, UA, DM Frame
* Im Betrieb werden erst alle ausstehenden UA und DM Frames gesendet, dann alle SABM frames beantwortet, dann werden erst weitere Frames gesendet
* XID Frames können jederzeit gesendet werden. Vor AX.25 v2.2 ist Antwort auf XID ein FRMR.
* mit XID Frames können 6 Parameter vereinbart werden:
 * Classes of Procedure, sowas wie Full Duplex/Half Duplex. Default ist Half-Duplex
 * HDLC Optional Functions, steuert Verhalten von REJ, SREJ, SREJ/REJ, dann modulo 8 vs modulo 128. Default REG, modulo 8
 * Einstellung der maximalen I-Field Length N1. Default 2048 bits
 * Maximale Window Size, gibt an wieviel Frames maximal gespeichert werden können (127 bei modulo 128). Default ist 7 
 * Acknowledge Timer parameter, wie lange auf ein ACK/UA gewartet wird. Default 3000milleseks
 * Retries parameter, wie oft maximal versucht wird, ein Paket erneut zu senden. Default 10 Versuche
* Nach Verbindungsaufbau geht TNC in den Informations-Transfer State über
* beim Empfang von SABM(E) während dieses Modus wird der Modus resetet
* in dem Modus kann jeder TNC mit DISC die Verbindung trennen
* nach eingegangen DISC Befehl sendet TNC ein UA und geht in den disconnected state. 
* nach Empfang von DM oder UA als Antwort auf ein DISC resetted der TNC auch den T1 Timer
* wenn der timer T1 vor Empfang von DM oder UA abläuft, wird das senden vom DISC wiederholt, bis der der Retries count erreicht ist.

disconnected mode:
* TNC beobachtet pakete
* reaktion auf SABM(E) Frames
* DM als Antwort empfangene DISC senden
* TNC kann selber SABM(E) senden
* Wenn TNC etwas anders als SABM(E) oder UI frame empfängt, wo P gleich '1' ist, antwortet TNC mit DM frame, wo P gleich '1' ist. Frame wird verworfen.
* Wenn der TNC wegen eines Fehlers irgendeiner Art in den disconnected mode geht, kann der TNC statt DISC auch ein DM frame senden

Collisionen:
* in Half Duplex Umgebung wird mit T1 und Retries versucht, auf Kollisionen zu reagieren.
* wenn gleichzeitig von zwei Stationen SABM(E) oder DISC gesendet wird, antworten beide mit UA Frames
* wenn gleichzeitig von einer Station SABM(E) und von der anderen DISC gesendet wird, gehen beide in den disconnected mode und senden DM frames
* DM frames werden immer mit P gleich '0' gesenden, SABM(E) und DISC werden mit P gleich '1' gesendet, DM ist also nie eine Antwort auf diese

Verbindungslose Übertragung
* Ohne Verbindungsaufbau
* Hierfür werden UI-Frames verwendet
* Als Zieladresse wird z.B. "PACKET" gewählt - nur Leute, die alles oder nur diese Zieladresse beobachten, erhalten die Nachrichten vom TNC
* Hier gibt es keine Retransmissionen, keine Antworten, kein Handshake

Imformations Transfer State
* Sobald eine Verbindung etabliert wurde
* Ein TNC kann dann ein I Frame senden. In das Controllfeld schreibt er den Wert aus dem aktuellen Sendezähler, nach dem Senden erhöht er den Zähler
* Nach dem Senden startet er den Timeout-Timer
* Er kann danach weitere Frames senden, solange die Maximal Window Size des Empfängers nicht überschritten wird
* Wenn der TNC gegenüber sich Busy meldet keine weiteren I-Frames aussenden
* Wenn ein TNC ein korrektes I-Frame empfängt, also FCS korrekt ist, und der Zähler mit dem Empfangszähler zusammenpasst, nimmt der TNC das Frame an:
 * Wenn der TNC selber ein I-Frame zum senden hat, kann er dieses senden mit angepassten Empfangszähler und seinen Info Field
 * Alternativ kann der TNC erst ein RR mit Empfangszähler senden, dann sein I-Frame
 * Wenn nichts zu senden ist, kann der TNC etwas warten und dann wird nur der RR Frame mit Empfangszähler gesendet
 * In der Wartezeit könnte der Sender ein weiteres I-Frame senden
* Wenn der TNC beschäftigt ist, kann er einfach alles ignorieren ohne Antwort, bzw sendet nur, dass er beschäftigt ist.
* Ein TNC, der Nachricht erhält, dass der andere TNC beschäftigt ist, wartet und fragt regelmässig an, bis der TNC nicht mehr beschäftigt ist
* Anfragen, ob ein TNC beschäftigt ist, geht mit RR oder RNR Frames, wo das P-bit '1' ist
* Frames mit P-Bit werden gesendet, bevor Frames mit P gleich '0' anstehen
* Wenn der Empfangszähler nicht passt, aber sonst ein I-Frame stimmt, wird das Paket verworfen und ein REJ Frame mit entsprechenen Wert gesendet. Der Wert ist eins höher, als das zuletzt empfangene Frame
* Alternativ kann das I-Frame gemerkt werden, und mit SREJ das oder die fehlenden Frames erneut abgefragt werden. 
 * Sollte kein SREJ aussteht, hat das P-bit den Wert '1'
 * Wenn weitere SREJ ausstehen, hat das p-bit des SREJ den Wert '0'
 * Queuing und Frame-neuanordnen sind dann notwendig
* Als dritte Variante REJ/SREJ: wenn mehrere Frames fehlen wird REJ gesendet, sonst SREJ wenn nur eine Frame fehlt.
* Frames mit falschem FCS werden verworfen, werden nicht quittiert
* Auch wenn TNC busy ist, überprüft er bei empfangenen Frames den Wert der empfangenen Pakete und stellt den Timer zurück wenn noch Frames unterwegs sind oder den Timer aus, wenn alle Frames angekommen sind. Wenn Timer abläuft, wird nach Ablauf des Busy-seins die Retransmission begonnen.
* Wenn ein TNC ein REJ erhält, sendet er alle I-frames nochmal, die angefragt wurden
 * wenn der TNC gerade nicht sendet, sofort senden
 * wenn der TNC im Full duplex modus ist und gerade ein UI oder S Frame sendet, kann er die Übertragung abschliessen und dann die I-Frames neu senden
 * wenn der TNC im Full duplex modus ist und gerade ein I Frame sendet, kann er die Übertragung abrechen und die I-Frames neu senden
 * der TNC kann soviele I-frames nachliefern, wie der Empfänger maximale Windowgröße hat
 * Wenn der TNC ein REJ mit P-bit erhält, antwortet er zuerst mit einem RR oder RNR frame mit P-bit gesetzt
* Wenn ein TNC ein SREJ erhält, sendet er das angefragt Frame erneut
* Wenn ein TNC ein RNR erhält, hört er auf zu senden bis der Empfänger nicht mehr busy ist
* Wenn ein weiterer Timer T3 abläuft, wird mit RR oder RNR mit gesetzten P-bit nach dem Status abgefragt, solange, bis busy vorrüber ist. 
* Wenn TNC busy ist, antwortet er mit RNR so bald wie möglich. S-Frames dürfen weiterhin empfangen und bearbeitet werden. Wenn ein S-frame mit P gleich 1 empfangen wird, kann mit RNR mit P gleich 1 geantwortet werden.
* Wenn ein TNC nicht mehr busy ist, kann er mit RR oder mit REJ mit dem letzten empfangenen Frame im Zähler antworten
* Wenn der Timer T1 abläuft ohne Antwort auf Frames, kann der TNC mit RR oder RNR mit P gleich 1 den empfänger abfragen
 * Wenn in der Antwort das P-bit gesetzt ist, der Empfangzähler höher wurde, wird der Zähler neu gestartet und der interne Zähler angepasst
 * Wenn in der Antwort nicht das p-bit gesetzt wird, läuft der Zähler weiter, interner Bestätigungszähler wird gegebenenfalls am Empfangszähler angepasst
 * Wenn keine rechtzeitige Antwort mit p-bit ankommt, wird das S-frame mit p-bit erneut gesendet. Nach N2 erfolglosen Versuchen wird die Verbindung getrennt
 
Resetten
* Reset wird gemacht wenn ein fehlerhaftes UA-Frame kommt, oder wenn ein FRMR von TNC mit altem AX.25 Standard kommt
* zuerst wird ein SABM(R) Framge gesendet mit Timer.
* Sobald möglich sollte ein UA Frame als Antwort kommen
* Alle Sende- und Empfangszähler werden auf 0 gesetzt
* Nach jedem erfolgreichen Frame wird der Timer neu gestartet
* Alle Empfangsbuffer der vorherigen Verbindung wird verworfen
* Wenn DM empfangen wurde, endet der Timer
* Wenn Timer T1 überläuft vor UA oder DM, wird erneut SABM(E) gesendet
* Wenn T1 N2 mal überläuft, ist der TNC im diconnected mode.
* Alle sonstigen Frames während des Resets werden verworfen
* ein TNC kann auch ein DM von anderen TNC anfordern, um danach in den disconnected mode zu gehen

Segmentation
* ein Disassembler/Reassembler kann größere Datenpakete in einzelen Frames aufteilen
* für Layer3 ist so übertragung von IP Paketen über AX.25 ab Version 2.2 möglich
* geht mit dem Segementation PID 00001000, gefolgt von einem Count-down Zähler, wieviele Segmente noch kommen







